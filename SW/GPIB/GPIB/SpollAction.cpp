/********************************************************************************
 Name:		TriggerAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 TriggerAction - ++trg command implementation (GPIB GET - Group Execute Trigger)
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "SpollAction.h"
#include "SettingCollection.h"

SpollAction::SpollAction(GPIB_MAC* gpib_mac)
{
	this->gpib_mac = gpib_mac;
}

void SpollAction::Execute(char* param)
{
	char* currParam;
	uint16_t ADDR;
	uint16_t SAD;
	uint8_t status;
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		this->gpib_mac->StartSpoll(setting_ADDR_OWN.Get());
		currParam = strtok(param, " \t");
		if (currParam == NULL)
		{
			// no address provided, use the the current remote address
			this->gpib_mac->ReadStatus(setting_ADDR_REM.Get(), &status);
			this->Print(setting_ADDR_REM.Get(), status);
		}
		else
		{
			do
			{
				ADDR = atoi(currParam) & 0x00FF;
				currParam = strtok(NULL, " \t");
				if (currParam != NULL)
				{
					SAD = atoi(currParam);
					if (SAD > 31)
					{
						ADDR |= SAD << 8;
						currParam = strtok(NULL, " \t");
					}
				}
				if ((ADDR & 0x00FF) != setting_ADDR_OWN.Get())
				{
					this->gpib_mac->ReadStatus(ADDR, &status);
					if (this->Print(ADDR, status))
					{
						currParam = NULL;
					}
				}
			} while (currParam != NULL);
		}
		this->gpib_mac->StopSpoll();
	}
}
bool SpollAction::Print(uint16_t address, uint8_t status)
{
	if (status & 0x40)
	{
		Serial.print("SRQ: ");
		Serial.print(address & 0x00FF);
		Serial.print(" ");
		if ((address & 0xFF00) != 0)
		{
			Serial.print(address >> 8);
			Serial.print(" ");
		}
		Serial.println(status);
		return true;
	}
	return false;
}