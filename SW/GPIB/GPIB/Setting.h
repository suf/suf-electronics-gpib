/********************************************************************************
 Name:		Setting.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Setting -  Setting base object and value store with one word in-memory backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include <stdint.h>

class Setting
{
public:
	Setting();
	Setting(uint16_t defaultValue, uint16_t min, uint16_t max);
	virtual bool begin();
	virtual int32_t Parse(char* param);
	virtual uint16_t Get();
	virtual int32_t Set(char* param);
	virtual int32_t Set(uint16_t value);
	virtual bool Validate(uint16_t value);
	virtual void InitStore();
	virtual void ErrorOutOfRange(uint8_t cmd_id);
	virtual void PrintSetting(uint8_t cmd_id);
	virtual void PrintValue();
	virtual void Save();
protected:
	uint16_t value;
	uint16_t defaultValue;
	uint16_t min;
	uint16_t max;
	bool isNull = false;
};

