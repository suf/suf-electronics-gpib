/********************************************************************************
 Name:		Command.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Command - Object for the ++ serial command triggered internal commands.
 Execution shell for the Settings and Actions
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include <stdint.h>
#include "Setting.h"
#include "Action.h"
class Command
{
public:
	/// Default constructor
	Command();
	/// cmdstr_id - Command string ID
	/// setting   - Setting object for the setting type commands
	/// action    - Action object for the action type commands
	Command(uint8_t cmdstr_id);
	Command(uint8_t cmdstr_id, Setting* setting);
	Command(uint8_t cmdstr_id, Action* action);
	Command(uint8_t cmdstr_id, Setting* setting, Action* action);
	/// Execute the setting and/or action
	/// param - string contains command line attributes
	virtual int32_t Execute(char* param);
	// Internally store the Command string ID, setting and action object
	uint8_t cmdstr_id;
	Setting* setting;
	Action* action;
};

