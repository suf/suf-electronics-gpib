/********************************************************************************
 Name:		Messages.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Messages - Collection of messages stored in progmem. Central place for localization
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include "Messages.h"

const char Message_000[] PROGMEM = "Error: ";
const char Message_001[] PROGMEM = " - Out of range. Valid value range [";
const char Message_002[] PROGMEM = "Log initialized";
const char Message_003[] PROGMEM = "Object Initialized";
const char Message_004[] PROGMEM = "NDAC timeout";
const char Message_005[] PROGMEM = "NRFD timeout";
const char Message_006[] PROGMEM = "Acknowledge timeout";
const char Message_007[] PROGMEM = "Byte Sent";
const char Message_008[] PROGMEM = "Invalid Mode";
const char Message_009[] PROGMEM = "Talker Address";
const char Message_010[] PROGMEM = "Listener Address";
const char Message_011[] PROGMEM = "The device not listening";

const char* const Messages[] PROGMEM = {
	Message_000,
	Message_001,
	Message_002,
	Message_003,
	Message_004,
	Message_005,
	Message_006,
	Message_007,
	Message_008,
	Message_009,
	Message_010,
	Message_011
};

void PrintMessage(uint8_t id)
{
	uint16_t strAddr = pgm_read_word(&Messages[id]);
	char buffer = 0;
	do
	{
		buffer = (char)pgm_read_byte(strAddr++);
		Serial.print(buffer);
	}
	while (buffer != '\0');
}