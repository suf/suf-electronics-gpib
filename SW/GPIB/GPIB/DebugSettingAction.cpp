/********************************************************************************
 Name:		AddressSettingAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 DebugSettingAction - Called by the ++dbg_setting command. In addition to save the
 setting to the EEPROM also set the level in the debug object.
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>

#include "DebugSettingAction.h"
#include "SettingCollection.h"
#include "debug.h"

 /// Default constructor
DebugSettingAction::DebugSettingAction(DEBUG* dbg)
{
	this->dbg = dbg;
}
/// Execute the action
/// param - string contains space separated address values. Address and Subaddress
void DebugSettingAction::Execute(char* param)
{
	this->dbg->SetDebugLevel(setting_DBG_LEVEL.Get());
}
