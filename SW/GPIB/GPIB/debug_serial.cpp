
/********************************************************************************
 Name:		debug_serial.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 debug_serial - Serial debug object
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "debug.h"
#include "debug_serial.h"
#include "Messages.h"

DEBUG_SERIAL::DEBUG_SERIAL()
{
  this->severity = LOG_SEVERITY_DEFAULT;
}

DEBUG_SERIAL::DEBUG_SERIAL(uint32_t baud)
{
	this->baud = baud;
	this->severity = LOG_SEVERITY_DEFAULT;
}

DEBUG_SERIAL::DEBUG_SERIAL(uint32_t baud, uint8_t severity)
{
  this->severity = severity;
  this->baud = baud;
}

void DEBUG_SERIAL::begin()
{
	Serial.begin(baud);
}

void DEBUG_SERIAL::begin(uint8_t severity)
{
	Serial.begin(baud);
	this->severity = severity;
}

void DEBUG_SERIAL::msg(uint8_t severity, char* message)
{
	char buffer[20];
	if (this->severity >= severity)
	{
		strcpy_P(buffer, (char*)pgm_read_word(&(DbgStr[severity])));
		Serial.println();
		Serial.print("[");
		Serial.print(buffer);
		Serial.print("] ");
		Serial.println(message);
	}
}

void DEBUG_SERIAL::msg(uint8_t severity, uint8_t msgid, uint8_t moduleId, uint8_t methodId)
{
	char buffer[20];
	if (this->severity >= severity)
	{
		strcpy_P(buffer, (char*)pgm_read_word(&(DbgStr[severity])));
		Serial.println();
		Serial.print("[");
		Serial.print(buffer);
		Serial.print("] ");
		this->PrintMethod(moduleId, methodId);
		Serial.print(": ");
		PrintMessage(msgid);
		Serial.println();
	}
}

void DEBUG_SERIAL::msg(uint8_t severity, uint8_t msgid, uint8_t moduleId, uint8_t methodId, int32_t value)
{
	char buffer[20];
	if (this->severity >= severity)
	{
		strcpy_P(buffer, (char*)pgm_read_word(&(DbgStr[severity])));
		Serial.println();
		Serial.print("[");
		Serial.print(buffer);
		Serial.print("] ");
		this->PrintMethod(moduleId, methodId);
		Serial.print(": ");
		PrintMessage(msgid);
		Serial.print(": ");
		Serial.println(value);
	}
}

void DEBUG_SERIAL::PrintMethod(uint8_t moduleId, uint8_t methodId)
{
	uint16_t strAddr = pgm_read_word(&DbgModule[moduleId]);
	char buffer = 0;
	do
	{
		buffer = (char)pgm_read_byte(strAddr++);
		Serial.print(buffer);
	} while (buffer != '\0');
	Serial.print("::");
	strAddr = pgm_read_word(&DbgMethod[methodId]);
	buffer = 0;
	do
	{
		buffer = (char)pgm_read_byte(strAddr++);
		Serial.print(buffer);
	} while (buffer != '\0');
}


void DEBUG_SERIAL::Clear()
{
  while (Serial.available() != 0)
  {
    Serial.read();
  }
}
