/********************************************************************************
 Name:		gpib_mac.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 gpib_mac - GPIB MAC (Media Access Control) layer - handle th physical communication
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "gpib_mac.h"
#include "hardware.h"
#include "ports.h"
#include "debug.h"
#include "Messages.h"

#ifndef GPIB_DATA_PORT
uint8_t gpib_data_pins[8]
{
	GPIB_DIO1,
	GPIB_DIO2,
	GPIB_DIO3,
	GPIB_DIO4,
	GPIB_DIO5,
	GPIB_DIO6,
	GPIB_DIO7,
	GPIB_DIO8
};
#endif

GPIB_MAC::GPIB_MAC()
{
	this->attn = true;
	this->debug = NULL;
}

GPIB_MAC::GPIB_MAC(DEBUG * dbg)
{
	this->attn = true;
	this->debug = dbg;
}

void GPIB_MAC::begin()
{
	this->SetDirection(GPIB_DIR_OFF, true);
	this->Log(LOG_SEVERITY_INFO, LOG_METHOD_begin, MESSAGE_Init);
}

uint8_t GPIB_MAC::SetMode(uint8_t mode)
{
	return this->SetMode(mode, false);
}

uint8_t GPIB_MAC::SetMode(uint8_t mode, bool force)
{
	if (mode < 4)
	{
		if (this->mode != mode || force)
		{
			this->mode = mode;
			pinMode(GPIB_SRQ, INPUT_PULLUP); // always input at initialization, only set to output when SRQ sent by a device
			pinMode(GPIB_EOI, INPUT_PULLUP); // direction will be changed based on attention
			// All Handshake set to input - will be changed based on the called functions
			pinMode(GPIB_DAV, INPUT_PULLUP);
			pinMode(GPIB_NDAC, INPUT_PULLUP);
			pinMode(GPIB_NRFD, INPUT_PULLUP);
			if (mode & GPIB_MODE_CTRL) // Controller
			{
				pinMode(GPIB_ATN, OUTPUT);
				this->EndAttention();
			}
			else // Device
			{
				pinMode(GPIB_ATN, INPUT_PULLUP);
			}
			if (mode & GPIB_MODE_SYSCTRL) // System Controller
			{
				pinMode(GPIB_IFC, OUTPUT);
				digitalWrite(GPIB_IFC, HIGH);
				pinMode(GPIB_REN, OUTPUT);
				digitalWrite(GPIB_REN, LOW); // Need to assert it at the begining???
			}
			else // System Controller Off
			{
				pinMode(GPIB_IFC, INPUT_PULLUP);
				pinMode(GPIB_REN, INPUT_PULLUP);
			}
		}
	}
	else
	{
		this->Log(LOG_SEVERITY_ERR, LOG_METHOD_SetMode, MESSAGE_Invalid_Mode);
		return 1; // Invalid mode
	}
	return 0;
}

uint8_t GPIB_MAC::GetMode()
{
	return this->mode;
}

// Worker called by the upper layers to poll the various
// requests from the bus
/*
void GPIB_MAC::Worker()
{
	if ((this->mode & 1) == GPIB_MODE_DEV)
	{
		// Device mode
		bool curr_atn = !digitalRead(GPIB_ATN);
		if (this->attn != curr_atn)
		{
			if (curr_atn) // Attention asserted
			{
				pinMode(GPIB_EOI, OUTPUT);
				digitalWrite(GPIB_EOI, HIGH);
			}
			else
			{
				pinMode(GPIB_EOI, INPUT_PULLUP);
			}
		}
	}
}
*/
// Controller mode send
uint8_t GPIB_MAC::SendByte(uint8_t data, bool isLast)
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		this->SetDirection(GPIB_DIR_OUTPUT);
		// Wait for bus to be ready
		if (this->Wait(GPIB_NDAC, LOW))
		{
			this->Log(LOG_SEVERITY_WARNING, LOG_METHOD_SendByte, MESSAGE_NDAC_Timeout);
			return GPIB_ERR_NDAC_TIMEOUT; // timeout
		}
		if (this->Wait(GPIB_NRFD, HIGH))
		{
			this->Log(LOG_SEVERITY_WARNING, LOG_METHOD_SendByte, MESSAGE_NRFD_Timeout);
			return GPIB_ERR_NRFD_TIMEOUT; // timeout
		}
		this->SetData(data);
		if (isLast)
		{
			digitalWrite(GPIB_EOI, LOW);
		}
		// Data available
		digitalWrite(GPIB_DAV, LOW);
		// Wait for acknowledge
		if (this->Wait(GPIB_NDAC, HIGH))
		{
			digitalWrite(GPIB_DAV, HIGH);
			if (isLast)
			{
				digitalWrite(GPIB_EOI, HIGH);
				this->SetDirection(GPIB_DIR_OFF);	// Release the bus
			}
			this->Log(LOG_SEVERITY_WARNING, LOG_METHOD_SendByte, MESSAGE_Ack_Timeout);
			return GPIB_ERR_SEND_ACK_TIMEOUT; // timeout
		}
		digitalWrite(GPIB_DAV, HIGH);
		if (isLast)
		{
			digitalWrite(GPIB_EOI, HIGH);
		//	this->SetDirection(GPIB_DIR_OFF);	// Release the bus
		}
		this->Log(LOG_SEVERITY_INFO, LOG_METHOD_SendByte, MESSAGE_Sent);
		return GPIB_OK;
	}
	this->Log(LOG_SEVERITY_ERR, LOG_METHOD_SendByte, MESSAGE_Invalid_Mode);
	return GPIB_ERR_Invalid_Mode;
}


uint8_t GPIB_MAC::StartSpoll(uint8_t ownAddr)
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Only the controller have permission to serial poll devices
		this->SetDirection(GPIB_DIR_OUTPUT);
		// Attention
		this->Attention();
		// Unlisten
		if (this->SendByte(GPIB_CMD_UNL, false))
		{
			return 1;
		}
		// Set myself to listen
		if (this->SendByte(GPIB_CMD_LISTEN + ownAddr, false))
		{
			return 1;
		}
		// Enable Serial Poll
		if (this->SendByte(GPIB_CMD_SPE, false))
		{
			return 1;
		}
		return 0;
	}
	return 1;
}
uint8_t GPIB_MAC::ReadStatus(uint16_t remAddr, uint8_t* data)
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Set the talker
		this->Attention();
		if (this->SendByte(GPIB_CMD_TALK + (remAddr & 0x00FF), false))
		{
			return 1;
		}
		// Set SAD
		if ((remAddr & 0xFF00) != 0)
		{
			if (this->SendByte(remAddr >> 8, false))
			{
				return 1;
			}
		}
		this->EndAttention();
		// Input from the bus
		this->SetDirection(GPIB_DIR_INPUT);
		if (this->GetData(data))
		{
			return 1;
		}
		this->SetDirection(GPIB_DIR_OUTPUT);
		return 0;
	}
	return 1;
}
uint8_t GPIB_MAC::StopSpoll()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		this->Attention();
		// Serial Poll Disable
		if (this->SendByte(GPIB_CMD_SPD, false))
		{
			return 1;
		}
		// Untalk
		if (this->SendByte(GPIB_CMD_UNT, false))
		{
			return 1;
		}
		// Unlisten
		if (this->SendByte(GPIB_CMD_UNL, false))
		{
			return 1;
		}
		this->EndAttention();
		return 0;
	}
	return 1;
}

// Send an unaddressed command
uint8_t GPIB_MAC::SendCommand(uint8_t command)
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Only the controller have permission to set address
		this->SetDirection(GPIB_DIR_OUTPUT);
		// Attention
		this->Attention();
		// Send command address
		if (this->SendByte(command, false))
		{
			return 1;
		}
		this->EndAttention();
		this->SetDirection(GPIB_DIR_OFF);
		return 0;
	}
	return 1;
}

// Send an adressed command
uint8_t GPIB_MAC::SendCommand(uint8_t command, uint16_t address)
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Only the controller have permission to set address
		this->SetDirection(GPIB_DIR_OUTPUT);
		// this->debug.msg(LOG_SEVERITY_INFO, "Output Mode");
		// Attention
		this->Attention();
		// Send target address
		if (this->SendByte(GPIB_CMD_LISTEN + (address & 0x00FF), false))
		{
			return 1;
		}
		if ((address & 0xFF00) != 0)
		{
			// SAD present
			if (this->SendByte(address >> 8, false))
			{
				return 1;
			}
		}
		// Send command
		if (this->SendByte(command, false))
		{
			return 1;
		}
		// Unlisten
		this->SendCommand(GPIB_CMD_UNL);
		return 0;
	}
	return 1;
}

uint8_t GPIB_MAC::SendAddress(uint16_t talker, uint16_t listener)
{
	uint8_t status;
	// this->debug.msg(LOG_SEVERITY_INFO, "SendAddress");
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Only the controller have permission to set address
		this->SetDirection(GPIB_DIR_OUTPUT);
		// Attention
		this->Attention();
		// Send talker address
		this->Log(LOG_SEVERITY_INFO, LOG_METHOD_SendAddress, MESSAGE_TalkerAddress, talker);
		status = this->SendByte(GPIB_CMD_TALK + (talker & 0x00FF), false);
		if (status > 0)
		{
			this->EndAttention();
			return status;
		}
		if ((talker & 0xFF00) != 0)
		{
			// SAD present
			status = this->SendByte(talker >> 8, false);
			if (status > 0)
			{
				this->EndAttention();
				return status;
			}
		}
		// Send listener address
		this->Log(LOG_SEVERITY_INFO, LOG_METHOD_SendAddress, MESSAGE_ListenerAddress, listener);
		status = this->SendByte(GPIB_CMD_LISTEN + (listener & 0x00FF), false);
		if (status > 0)
		{
			this->EndAttention();
			return status;
		}
		if ((listener & 0xFF00) != 0)
		{
			// SAD present
			status = this->SendByte(listener >> 8, false);
		}
		this->EndAttention();
		return status;
	}
	this->Log(LOG_SEVERITY_ERR, LOG_METHOD_SendAddress, MESSAGE_Invalid_Mode);
	return GPIB_ERR_Invalid_Mode;
}

uint8_t GPIB_MAC::GetSRQ()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Controller mode
		return digitalRead(GPIB_SRQ) ? 0 : 1;
	}
	return 0;
}

/// Check if the device remains on the bus after address sent
/// If the NDAC not kept pull down, means the device is not listening (probably not there)
uint8_t GPIB_MAC::IsListening()
{
	delayMicroseconds(10); // Wait for the bus to stabilize
	if (this->Wait(GPIB_NDAC, LOW))
	{
		this->Log(LOG_SEVERITY_WARNING,LOG_METHOD_IsListening, MESSAGE_Not_Listening);
		return GPIB_ERR_NDAC_TIMEOUT; // timeout
	}
	return GPIB_OK;
}

bool GPIB_MAC::Available()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Controller mode
		return !digitalRead(GPIB_DAV);
	}
	else
	{
		// Device mode
	}
	return false;
}

void GPIB_MAC::StartRead()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Controller mode
		this->SetDirection(GPIB_DIR_INPUT);
		if (this->Wait(GPIB_DAV, HIGH))
		{
			// this->debug->msg(LOG_SEVERITY_WARNING, MESSAGE_GPIB_SendByte_NDAC_Timeout);
			// return GPIB_ERR_NDAC_TIMEOUT; // timeout
			return;
		}
		digitalWrite(GPIB_NDAC, LOW);
		digitalWrite(GPIB_NRFD, HIGH);
	}
	else
	{
		// Device mode
	}
}

uint16_t GPIB_MAC::Read()
{
	int i;
	uint8_t buff;
	uint16_t retValue = 0;
	uint8_t eoi;
	if (this->mode & GPIB_MODE_CTRL)
	{
		// Controller mode
		digitalWrite(GPIB_NRFD, LOW);
		// delayMicroseconds(10);
		this->GetData(&buff);
		eoi = digitalRead(GPIB_EOI);
		retValue = buff + ((eoi) ? 0 : 0x0100); // Signal the caller about end of message
		// cr, lf, 0 can be used instead of eoi ???
		digitalWrite(GPIB_NDAC, HIGH);
		// End of message, release the bus
		if (eoi == LOW)
		{
			this->SetDirection(GPIB_DIR_OFF);
		}
	}
	else
	{
		// Device mode
	}
	return retValue;
}


// Bus Reset (IFC - Interface Clear)
uint8_t GPIB_MAC::IFC()
{
	if (this->mode & 2) // System Controler bit set
	{
		pinMode(GPIB_IFC, OUTPUT);
		digitalWrite(GPIB_IFC, LOW);
		delayMicroseconds(GPIB_CTRL_TIME_us);
		digitalWrite(GPIB_IFC, HIGH);
		return 0;
	}
	return 1; // Error - IFC can be initiated only by the System Controler
}

// Internal implementation of the debug log
void GPIB_MAC::Log(uint8_t severity, uint8_t methodId, uint8_t msgid)
{
	if (this->debug != NULL)
	{
		this->debug->msg(severity, msgid, LOG_MODULE_GPIB_MAC, methodId);
	}
}

void GPIB_MAC::Log(uint8_t severity, uint8_t methodId, uint8_t msgid, int32_t value)
{
	if (this->debug != NULL)
	{
		this->debug->msg(severity, msgid, LOG_MODULE_GPIB_MAC, methodId,  value);
	}
}

bool GPIB_MAC::Wait(uint8_t pin, uint8_t level)
{
	return this->Wait(pin, level, DEFAULT_IO_TIMEOUT);
}

bool GPIB_MAC::Wait(uint8_t pin, uint8_t level, uint16_t timeout)
{
	uint32_t expire_time;
	pinMode(pin, INPUT_PULLUP);
	expire_time = millis() + timeout;
	while (expire_time > millis())
	{
		if (digitalRead(pin) == level)
		{
			return false;
		}
	}
	return true; // timeout
}

int8_t GPIB_MAC::SetDirection(uint8_t direction)
{
	return this->SetDirection(direction, false);
}

int8_t GPIB_MAC::SetDirection(uint8_t direction, bool force)
{
	uint8_t i;
	if (this->direction != direction || force)
	{
		this->direction = direction;
		switch (direction)
		{
		case GPIB_DIR_OFF:
		case GPIB_DIR_INPUT:
    
#ifdef GPIB_DATA_PORT
			GPIB_DATA_REG_DDR = 0;	// Input
			GPIB_DATA_REG_PORT = 0xFF; // Pull up
#else
			// if separate MCU pins used, address it one by one
			for (i = 0; i < 8; i++)
			{
				pinMode(gpib_data_pins[i], INPUT_PULLUP);
			}
#endif
			pinMode(GPIB_EOI, INPUT_PULLUP);
			pinMode(GPIB_DAV, INPUT_PULLUP);
			pinMode(GPIB_NRFD, direction == GPIB_DIR_OFF ? INPUT_PULLUP : OUTPUT);
			pinMode(GPIB_NDAC, direction == GPIB_DIR_OFF ? INPUT_PULLUP : OUTPUT);
			if (direction == GPIB_DIR_INPUT)
			{
				digitalWrite(GPIB_NRFD, LOW);
				digitalWrite(GPIB_NDAC, HIGH);
			}
			pinMode(GPIB_SRQ, INPUT_PULLUP);
			break;
		case GPIB_DIR_OUTPUT:
#ifdef GPIB_DATA_PORT
			// if a full 8 bit MCU port used, write to it directly
			// Output direction
			GPIB_DATA_REG_DDR = 0xFF;
			// Write to port
			GPIB_DATA_REG_PORT = 0xFF;
#else
			// if separate MCU pins used, address it one by one
			for (i = 0; i < 8; i++)
			{
				pinMode(gpib_data_pins[i], OUTPUT);
				digitalWrite(gpib_data_pins[i], HIGH);
				// this->SetData(0xFF);
			}
#endif
			pinMode(GPIB_EOI, OUTPUT);
			pinMode(GPIB_DAV, OUTPUT);
			pinMode(GPIB_NRFD, INPUT_PULLUP);
			pinMode(GPIB_NDAC, INPUT_PULLUP);
			digitalWrite(GPIB_EOI, HIGH);
			digitalWrite(GPIB_DAV, HIGH);
      break;
		default:
			return 1;
		}
	}
	return 0;
}
uint8_t GPIB_MAC::SetData(uint8_t data)
{
  int i;
	if (this->direction == GPIB_DIR_OUTPUT)
	{
		// direct port manipulation
#ifdef GPIB_DATA_PORT
		// if a full 8 bit MCU port used, write to it directly
		// Write to port
		GPIB_DATA_REG_PORT = ~data;
#else
		// if separate MCU pins used, address it one by one
		for (i = 0; i < 8; i++)
		{
		  digitalWrite(gpib_data_pins[i], (~data >> i) & 0x01);
		}
#endif
		return 0;
	}
	return 1;
}

uint8_t GPIB_MAC::GetData(uint8_t* data)
{
	if (this->direction == GPIB_DIR_INPUT)
	{
#ifdef GPIB_DATA_PORT
		* data = GPIB_DATA_REG_PIN; // Read
#else
		// if separate MCU pins used, address it one by one
		int i;
		*data = 0;
		for (i = 7; i >= 0; i--)
		{
			*data <<= 1;
			*data |= digitalRead(gpib_data_pins[i]) ? 0 : 1;;
		}
		// *data = ~*data;
#endif
		return 0;
	}
	return 1;
}

uint8_t GPIB_MAC::Attention()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		if (!this->attn)
		{
			digitalWrite(GPIB_ATN, LOW);
			this->attn = true;
			pinMode(GPIB_EOI, INPUT_PULLUP);
			// this->debug.msg(LOG_SEVERITY_INFO, "Attention Asserted");
		}
		return 0;
	}
	return 1;
}

uint8_t GPIB_MAC::EndAttention()
{
	if (this->mode & GPIB_MODE_CTRL)
	{
		if (this->attn)
		{
			digitalWrite(GPIB_ATN, HIGH);
			this->attn = false;
			pinMode(GPIB_EOI, OUTPUT);
			digitalWrite(GPIB_EOI, HIGH);
			// this->debug.msg(LOG_SEVERITY_INFO, "Attention Unasserted");
		}
		return 0;
	}
	return 1;
}
