/********************************************************************************
 Name:		EEPROMSetting16.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 EEPROMSetting16 - Setting value store with one word EEPROM backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>
#include <EEPROM.h>

#include "Setting.h"
#include "EEPROMSetting16.h"
#include "SettingCollection.h"


EEPROMSetting16::EEPROMSetting16()
{
}

EEPROMSetting16::EEPROMSetting16(uint8_t addr, uint16_t defaultValue, uint16_t min, uint16_t max)
{
	this->EEPROMaddr = addr;
	this->defaultValue = defaultValue;
	this->min = min;
	this->max = max;
}

bool EEPROMSetting16::begin()
{
	uint16_t tmpValue;
	tmpValue = EEPROM.read(this->EEPROMaddr + 1);
	tmpValue <<= 8;
	tmpValue += EEPROM.read(this->EEPROMaddr);
	if (this->Validate(tmpValue))
	{
		this->value = tmpValue;
		return true;
	}
	else
	{
		this->value = this->defaultValue;
	}
	return false;
}

void EEPROMSetting16::InitStore()
{
	this->value = this->defaultValue;
	this->Save();
}

int32_t EEPROMSetting16::Set(uint16_t value)
{
	// Set it in the object itself
	int32_t IntValue = Setting::Set(value);
	// If autosave enabled, save it also to the EEPROM
	if (IntValue >= 0 && setting_AUTOSAVE.Get() == 1)
	{
		this->Save();
	}
	return IntValue;
}

int32_t EEPROMSetting16::Set(char* param)
{
	return this->Set(this->Parse(param));
}

void EEPROMSetting16::Save()
{
	EEPROM.update(this->EEPROMaddr, this->value & 0xFF);
	EEPROM.update(this->EEPROMaddr + 1, (this->value >> 8) & 0xFF);
}
