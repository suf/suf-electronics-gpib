/********************************************************************************
 Name:		debug.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 debug - Base object for debug logging. Only serial logging implemented so far
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "debug.h"

const char DbgStr_EMERGENCY[] PROGMEM = "EMERGENCY";
const char DbgStr_ALERT[] PROGMEM = "ALERT";
const char DbgStr_CRITICAL[] PROGMEM = "CRITICAL";
const char DbgStr_ERROR[] PROGMEM = "ERROR";
const char DbgStr_WARNING[] PROGMEM = "WARNING";
const char DbgStr_NOTICE[] PROGMEM = "NOTICE";
const char DbgStr_INFORMATION[] PROGMEM = "INFORMATION";
const char DbgStr_DEBUG[] PROGMEM = "DEBUG";

const char* const DbgStr[] PROGMEM = {
	DbgStr_EMERGENCY,
	DbgStr_ALERT,
	DbgStr_CRITICAL,
	DbgStr_ERROR,
	DbgStr_WARNING,
	DbgStr_NOTICE,
	DbgStr_INFORMATION,
	DbgStr_DEBUG
};

const char DbgModule_gpib_mac[] PROGMEM = "GPIB_MAC";
const char DbgModule_main[] PROGMEM = "MAIN";
const char DbgModule_gpib_app[] PROGMEM = "GPIB_APP";

const char* const DbgModule[] PROGMEM = {
	DbgModule_gpib_mac,
	DbgModule_main,
	DbgModule_gpib_app
};

const char DbgMethod_SetMode[] PROGMEM = "SetMode";
const char DbgMethod_SendByte[] PROGMEM = "SendByte";
const char DbgMethod_SendAddress[] PROGMEM = "SendAddress";
const char DbgMethod_IsListening[] PROGMEM = "IsListening";
const char DbgMethod_begin[] PROGMEM = "begin";
const char DbgMethod_setup[] PROGMEM = "setup";
const char DbgMethod_loop[] PROGMEM = "loop";

const char* const DbgMethod[] PROGMEM = {
	DbgMethod_SetMode,
	DbgMethod_SendByte,
	DbgMethod_SendAddress,
	DbgMethod_IsListening,
	DbgMethod_begin,
	DbgMethod_setup,
	DbgMethod_loop
};

 
DEBUG::DEBUG() {}
void DEBUG::msg(uint8_t severity, char* message) {}
void DEBUG::msg(uint8_t severity, uint8_t modId, uint8_t methodId, uint8_t msgid) {}
void DEBUG::msg(uint8_t severity, uint8_t modId, uint8_t methodId, uint8_t msgid, int32_t value) {}
void DEBUG::PrintMethod(uint8_t moduleId, uint8_t methodId) {}

void DEBUG::SetDebugLevel(uint8_t severity)
{
	this->severity = severity;
}

