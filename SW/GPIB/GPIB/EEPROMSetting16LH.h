/********************************************************************************
 Name:		EEPROMSetting16LH.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 EEPROMSetting16LH - Setting value store with one word (separate low/high) EEPROM backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include <stdint.h>
#include "Setting.h"
class EEPROMSetting16LH : public Setting
{
public:
	EEPROMSetting16LH();
	EEPROMSetting16LH(uint8_t addr, uint8_t def_low, uint8_t def_high);
	virtual bool begin();
	virtual void InitStore();
	int32_t Parse(char* param);
	int32_t Set(uint8_t value_low, uint8_t value_high);
	int32_t Set(uint16_t value);
	int32_t Set(char* param);
	void PrintValue();
	void Save();
protected:
	uint8_t EEPROMaddr;
	uint8_t def_low;
	uint8_t def_high;
};

