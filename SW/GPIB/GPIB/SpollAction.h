/********************************************************************************
 Name:		SpollAction.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 SpollAction - ++spoll command implementation (Serial Poll)
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include <stdint.h>
#include "Action.h"
#include "gpib_mac.h"
class SpollAction : public Action
{
public:
	SpollAction(GPIB_MAC* gpib_mac);
	void Execute(char* param);
private:
	bool Print(uint16_t address, uint8_t status);
	GPIB_MAC* gpib_mac;
};

