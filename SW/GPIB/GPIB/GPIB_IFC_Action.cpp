/********************************************************************************
 Name:		GPIB_IFC_Action.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 GPIB_IFC_Action - ++ifc command implementation. Send an IFC (Interface Clear)
 command to the bus
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include "GPIB_IFC_Action.h"
#include "gpib_mac.h"
#include "SettingCollection.h"


GPIB_IFC_Action::GPIB_IFC_Action(GPIB_MAC* gpib_mac)
{
	this->gpib_mac = gpib_mac;
}

void GPIB_IFC_Action::Execute(char* param)
{
	if (this->gpib_mac->GetMode() & GPIB_MODE_SYSCTRL)
	{
		this->gpib_mac->IFC();
	}
}
