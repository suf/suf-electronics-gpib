/********************************************************************************
 Name:		SaveSettingsAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 SaveSettingsAction - ++savecfg command implementation. Save the settings to the
 EEPROM
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "SaveSettingsAction.h"
#include "SettingCollection.h"
#include "GPIB_APP.h"


SaveSettingsAction::SaveSettingsAction()
{
}

void SaveSettingsAction::Execute(char* param)
{
	if (setting_AUTOSAVE.Get() > 0)
	{
		for (uint8_t i; i < sizeof(commands); i++)
		{
			if (commands[i].setting != NULL)
			{
				commands[i].setting->Save();
			}
		}
	}
}

