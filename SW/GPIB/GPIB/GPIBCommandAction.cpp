/********************************************************************************
 Name:		GPIBCommandAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 GPIBCommandAction - Multiline addressed and unaddressed GPIB command implementation
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "GPIBCommandAction.h"
#include "SettingCollection.h"

GPIBCommandAction::GPIBCommandAction(GPIB_MAC* gpib_mac, uint8_t cmd, uint8_t cmdmode, uint8_t cmdtype)
{
	this->cmd = cmd;
	this->cmdmode = cmdmode;
	this->cmdtype = cmdtype;
	this->gpib_mac = gpib_mac;
}

void GPIBCommandAction::Execute(char* param)
{
	if (this->gpib_mac->GetMode() & this->cmdmode)
	{
		switch (this->cmdtype)
		{
		case GPIB_CMD_UNADDRESSED:
			this->gpib_mac->SendCommand(this->cmd);
			break;
		case GPIB_CMD_ADDRESSED:
			this->gpib_mac->SendCommand(this->cmd, setting_ADDR_REM.Get());
			break;
		default:
			break;
		}
	}
}
