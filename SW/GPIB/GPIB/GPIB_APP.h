/********************************************************************************
 Name:		GPIB_APP.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 GPIB_APP - Main application object. Handle the serial commands and the GPIB communication
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/

#pragma once
#include <Arduino.h>
#include <stdint.h>

#include "debug.h"
#include "Command.h"
#include "gpib_mac.h"

#define SERIAL_BUFF_LEN 32

#define ESC 92	// Backslash - will be replaced by Escape char
#define BS 8
#define CR 13
#define LF 10

#define CMD_addr		0
#define CMD_auto		1
#define CMD_clr			2
#define CMD_eoi			3
#define CMD_eos			4
#define CMD_eot_enable	5
#define CMD_eot_char	6
#define CMD_ifc			7
#define CMD_llo			8
#define CMD_loc			9
#define CMD_lon			10
#define CMD_mode		11
#define CMD_read		12
#define CMD_read_tmo_ms	13
#define CMD_rst			14
#define CMD_savecfg		15
#define CMD_spoll		16
#define CMD_srq			17
#define CMD_status		18
#define CMD_trg			19
#define CMD_ver			20
#define CMD_help		21

#define CMD_dcl			22
#define CMD_verbose		23

#define CMD_addr_rem	24
#define CMD_addr_own	25
#define CMD_dbg_level	26
#define CMD_echo		27
#define CMD_tct			28
#define CMD_eeprom_init	29
#define CMD_emulate_ver 30
#define CMD_lon_format		31
#define CMD_send_srq	32
#define CMD_list_settings	33
#define CMD_scan			34
#define CMD_read_hex			35
 // #define CMD_sys_ctrl		36

#define CmdArr_len			36

extern Command commands[CmdArr_len];
extern const char* const CmdStr[];

class GPIB_APP
{
public:
	// Constructor
	GPIB_APP(GPIB_MAC* gpib_mac, DEBUG* log);
	// Object initialization
	void begin();
	// Serial character worker, called from the main loop
	void ReceiveCharSerial();
	// GPIB data receiver worker, called from the main loop
	void GPIBListenWorker();
	// Send a string to GPIB
	uint8_t GPIBSendString();
	// Start the receive process (enable the GPIBListenWorker)
	uint8_t GPIBStartReceive();
	uint8_t GPIBStartReceive(bool IsParam, bool eoi, uint8_t EndChar);
	uint8_t GPIBStartReceive(bool IsParam, bool eoi, uint8_t EndChar, bool hex);
	uint8_t GPIBStartReceive(uint16_t rem_addr, bool IsParam, bool eoi, uint8_t EndChar, bool hex);
	void GPIBRead(uint16_t rem_addr, bool IsParam, bool eoi, uint8_t EndChar, bool hex);
	bool GPIBIsListening();
private:
	void Log(uint8_t severity, uint8_t methodId, uint8_t msgid);
	void ProcessCommand(char* cmd, char* params);
	char linebuff[SERIAL_BUFF_LEN];
	uint8_t linebuff_pos;
	// listen mode switch: on in Listen only mode, Device mode, 
	// and when a read command asserted in Controller mode
	bool read_listen;
	bool read_IsParam;
	bool read_eoi;
	bool read_hex;
	bool read_firstchar;
	uint8_t read_EndChar;
	DEBUG* log;
	GPIB_MAC* gpib_mac;
	uint32_t read_timeout;
};
