/********************************************************************************
 Name:		EEPROMSetting8.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 EEPROMSetting8 - Setting value store with one byte EEPROM backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include <stdint.h>
#include "Setting.h"
class EEPROMSetting8 : public Setting
{
public:
	EEPROMSetting8();
	EEPROMSetting8(uint8_t addr, uint8_t defaultValue, uint8_t min, uint8_t max);
	virtual bool begin();
	virtual void InitStore();
	int32_t Set(uint16_t value);
	int32_t Set(char* param);
	void Save();
private:
	uint8_t EEPROMaddr;
};

