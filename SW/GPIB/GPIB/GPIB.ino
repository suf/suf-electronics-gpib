/********************************************************************************
 Name:		GPIB.ino
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>
#include <EEPROM.h>

#include "hardware.h"
#include "ports.h"

#include "GPIB.h"

#include "debug.h"
#include "debug_serial.h"
#include "gpib_mac.h"

#include "SettingCollection.h"
#include "GPIB_APP.h"

#include "Messages.h"

// Serial loging object
DEBUG_SERIAL slog(115200, LOG_SEVERITY_DEBUG);
// GPIB Media Access Control
GPIB_MAC gpib_mac(&slog);
// GPIB Application
GPIB_APP gpib_app(&gpib_mac, &slog);

// the setup function runs once when you press reset or power the board
void setup()
{
	setting_DBG_LEVEL.begin();
	// Start logging
	slog.begin(setting_DBG_LEVEL.Get());
	slog.msg(LOG_SEVERITY_INFO, MESSAGE_Log_Init, LOG_MODULE_MAIN, LOG_METHOD_setup);
	// Set the GPIB interface mode to the EEPROM Stored Mode
	// Additional settings will be needed (like LON mode)
	setting_MODE.begin();
	action_MODE_SETTING.Execute();
	// Start the MAC
	gpib_mac.begin();
	// Start the application
	gpib_app.begin();
	slog.msg(LOG_SEVERITY_INFO, MESSAGE_Init, LOG_MODULE_MAIN, LOG_METHOD_setup);
}

// the loop function runs over and over again until power down or reset
void loop() 
{
	// Reading from serial and GPIB interface control the show
	// Read from serial
	if (Serial.available())
	{
		gpib_app.ReceiveCharSerial();
	}
	// Read from GPIB
	gpib_app.GPIBListenWorker();
}
