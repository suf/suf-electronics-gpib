/********************************************************************************
 Name:		gpib_mac.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 gpib_mac - GPIB MAC (Media Access Control) layer - handle th physical communication
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/

#pragma once

#include <Arduino.h>
#include <stdint.h>

#include "debug.h"

// the 150uS comming from the Prologix documentation
#define GPIB_CTRL_TIME_us	150 

#define GPIB_DIR_OFF 0
#define GPIB_DIR_INPUT 1
#define GPIB_DIR_OUTPUT 2

#define GPIB_MODE_SYSCTRL 2
#define GPIB_MODE_DEV 0
#define GPIB_MODE_CTRL 1

// Addressed commands
#define GPIB_CMD_GTL	 1	// Go To Local
#define GPIB_CMD_SDC	 4	// Selective Device Clear
#define GPIB_CMD_PPC	 5	// Parallel Poll Configure
#define GPIB_CMD_GET	 8
#define GPIB_CMD_TCT	 9	// Take Control
// Unaddressed commands
#define GPIB_CMD_LLO	17	// Local Lockout
#define GPIB_CMD_DCL	20	// Device Clear
#define GPIB_CMD_PPU	21	// Parallel Poll Unconfigure
#define GPIB_CMD_SPE	24	// Serial Poll Enable
#define GPIB_CMD_SPD	25	// Serial Poll Disable
#define GPIB_CMD_UNL	63	// Unlisten
#define GPIB_CMD_LISTEN 32	// Addressing the listener. Add the address to it (0-30)
#define GPIB_CMD_TALK	64	// Addressing the talker. Add the address to it (0-30)
#define GPIB_CMD_UNT	95	// Untalk

#define GPIB_CMD_UNADDRESSED	0
#define GPIB_CMD_ADDRESSED		1

#define GPIB_OK	0
#define GPIB_ERR_NDAC_TIMEOUT 1
#define GPIB_ERR_NRFD_TIMEOUT 2
#define GPIB_ERR_SEND_ACK_TIMEOUT 3
#define GPIB_ERR_Invalid_Mode 4

class GPIB_MAC
{
public:
	// Constructor
	GPIB_MAC();
	GPIB_MAC(DEBUG* dbg);
	// Start
	void begin();

	uint8_t SetMode(uint8_t mode);
	uint8_t SetMode(uint8_t mode, bool force);
	uint8_t GetMode();
	// Controller mode commands
	uint8_t SendByte(uint8_t data, bool isLast);
	uint8_t StartSpoll(uint8_t ownAddr);
	uint8_t ReadStatus(uint16_t remAddr, uint8_t* data);
	uint8_t StopSpoll();
	uint8_t SendCommand(uint8_t command);
	uint8_t SendCommand(uint8_t command, uint16_t address);
	uint8_t SendAddress(uint16_t talker, uint16_t listener);
	uint8_t GetSRQ();

	uint8_t IsListening();

	bool Available();
	void StartRead();
	uint16_t Read();

	// System Controller commands
	uint8_t IFC();
	// Device mode commands
	// Responde
	// Listen
	// The "Listen" maybe polled or based on interrupt
	// Interface Diagnostic
private:
	void Log(uint8_t severity, uint8_t methodId, uint8_t msgid);
	void Log(uint8_t severity, uint8_t methodId, uint8_t msgid, int32_t value);
	bool Wait(uint8_t pin, uint8_t level);
	bool Wait(uint8_t pin, uint8_t level, uint16_t timeout);
	int8_t SetDirection(uint8_t direction);
	int8_t SetDirection(uint8_t direction, bool force);
	uint8_t SetData(uint8_t data); // Just set the data on the bus
	uint8_t GetData(uint8_t* data); // Just get what is on the bus
	uint8_t Attention();
	uint8_t EndAttention();
	// variables
	uint8_t mode; // Device Mode Bit 0 - Controller (1) / Device (0), Bit 1 - System Controler (1)
	uint8_t direction;
	bool attn;
	bool available;
    DEBUG* debug;
};
