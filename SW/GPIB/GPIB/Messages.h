/********************************************************************************
 Name:		Messages.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Messages - Collection of messages stored in progmem. Central place for localization
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once

void PrintMessage(uint8_t id);

#define MESSAGE_Error_colon 0
#define MESSAGE_Out_of_range 1
#define MESSAGE_Log_Init 2
#define MESSAGE_Init 3
#define MESSAGE_NDAC_Timeout 4
#define MESSAGE_NRFD_Timeout 5
#define MESSAGE_Ack_Timeout 6
#define MESSAGE_Sent 7
#define MESSAGE_Invalid_Mode 8
#define MESSAGE_TalkerAddress 9
#define MESSAGE_ListenerAddress 10
#define MESSAGE_Not_Listening 11

extern const char* const Messages[];