/********************************************************************************
 Name:		TriggerAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 TriggerAction - ++trg command implementation (GPIB GET - Group Execute Trigger)
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "TriggerAction.h"
#include "SettingCollection.h"

TriggerAction::TriggerAction(GPIB_MAC* gpib_mac)
{
	this->gpib_mac = gpib_mac;
}

void TriggerAction::Execute(char* param)
{
	char* currParam;
	uint16_t ADDR;
	uint16_t SAD;
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		currParam = strtok(param, " \t");
		if (currParam == NULL)
		{
			// no address provided, use the the current remote address
			this->GET(setting_ADDR_REM.Get());
		}
		else
		{
			do
			{
				ADDR = atoi(currParam) & 0x00FF;
				currParam = strtok(NULL, " \t");
				if (currParam != NULL)
				{
					SAD = atoi(currParam);
					if (SAD > 31)
					{
						ADDR |= SAD << 8;
						currParam = strtok(NULL, " \t");
					}
				}
				if ((ADDR & 0x00FF) != setting_ADDR_OWN.Get())
				{
					this->GET(ADDR);
				}
			} while (currParam != NULL);
		}
	}
}

void TriggerAction::GET(uint16_t Address)
{
	// SAD not yet handled
	this->gpib_mac->SendCommand(GPIB_CMD_GET, Address);
}
