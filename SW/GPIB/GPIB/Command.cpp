/********************************************************************************
 Name:		Command.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Command - Object for the ++ serial command triggered internal commands.
 Execution shell for the Settings and Actions
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "Command.h"
#include "Action.h"
#include "Setting.h"

// Constructors. Save only the calling parameters
// If either the setting or the action object not added, using NULL object instead
// NULL object checked, and not called.
Command::Command()
{
	this->action = NULL;
	this->setting = NULL;
}

Command::Command(uint8_t cmdstr_id)
{
	this->cmdstr_id = cmdstr_id;
	this->action = NULL;
	this->setting = NULL;
}

Command::Command(uint8_t cmdstr_id, Setting* setting)
{
	this->cmdstr_id = cmdstr_id;
	this->action = NULL;
	this->setting = setting;
}

Command::Command(uint8_t cmdstr_id, Action* action)
{
	this->cmdstr_id = cmdstr_id;
	this->setting = NULL;
	this->action = action;
}

Command::Command(uint8_t cmdstr_id, Setting* setting, Action* action)
{
	this->cmdstr_id = cmdstr_id;
	this->setting = setting;
	this->action = action;
}

/// Execute the setting/action
/// param - string contains command line attributes
int32_t Command::Execute(char* param)
{
	int32_t value = 0;
	// Setting command
	if (this->setting != NULL)
	{
		if (param != NULL)
		{
			// Set
			value = this->setting->Set(param);
			if (value < 0)
			{
				this->setting->ErrorOutOfRange(this->cmdstr_id);
			}
		}
		else
		{
			// Setting command without parameters just print the current value
			this->setting->PrintValue();
		}
	}
	// Action command
	if (this->action != NULL)
	{
		this->action->Execute(param);
	}
	return value;
}
