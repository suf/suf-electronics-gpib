/********************************************************************************
 Name:		ports.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Arduino physical port definition
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>

// ATmega328P
#if defined(__AVR_ATmega328P__)

#define GPIB_DIO1  A0  // GPIB 1  : I/O data bit 1     - MCU C0
#define GPIB_DIO2  A1  // GPIB 2  : I/O data bit 2     - MCU C1
#define GPIB_DIO3  A2  // GPIB 3  : I/O data bit 3     - MCU C2
#define GPIB_DIO4  A3  // GPIB 4  : I/O data bit 4     - MCU C3
#define GPIB_EOI   12  // GPIB 5  : End Or Identify    - MCU B4
#define GPIB_DAV   11  // GPIB 6  : Data Valid         - MCU B3
#define GPIB_NRFD  10  // GPIB 7  : Not Ready For Data - MCU B2
#define GPIB_NDAC  9   // GPIB 8  : Not Data Accepted  - MCU B0
#define GPIB_IFC   8   // GPIB 9  : Interface Clear    - MCU B1
#define GPIB_SRQ   2   // GPIB 10 : Service request    - MCU D2
#define GPIB_ATN   7   // GPIB 11 : Attention          - MCU D7
#define GPIB_DIO5  A4  // GPIB 13 : I/O data bit 5     - MCU C4
#define GPIB_DIO6  A5  // GPIB 14 : I/O data bit 6     - MCU C5
#define GPIB_DIO7  4   // GPIB 15 : I/O data bit 7     - MCU D4
#define GPIB_DIO8  5   // GPIB 16 : I/O data bit 8     - MCU D5
#define GPIB_REN   3   // GPIB 17 : Remote enable      - MCU D3

#endif

// ATmega32U4
#if defined(__AVR_ATmega32U4__)

#define GPIB_DATA_PORT B

#define GPIB_DR_PASTER(reg, port) reg ## port
#define GPIB_DR_EVAL(reg, port) GPIB_DR_PASTER(reg, port)
#define GPIB_DR(reg) GPIB_DR_EVAL(reg, GPIB_DATA_PORT)

#define GPIB_DATA_REG_PORT GPIB_DR(PORT)
#define GPIB_DATA_REG_DDR GPIB_DR(DDR)
#define GPIB_DATA_REG_PIN GPIB_DR(PIN)

// #define GPIB_DIO1  17  // GPIB 1  : I/O data bit 1     - MCU B0
// #define GPIB_DIO2  15  // GPIB 2  : I/O data bit 2     - MCU B1
// #define GPIB_DIO3  16  // GPIB 3  : I/O data bit 3     - MCU B2
// #define GPIB_DIO4  14  // GPIB 4  : I/O data bit 4     - MCU B3
#define GPIB_EOI   7   // GPIB 5  : End Or Identify    - MCU E6
#define GPIB_DAV   3   // GPIB 6  : Data Valid         - MCU D0
#define GPIB_NRFD  2   // GPIB 7  : Not Ready For Data - MCU D1
#define GPIB_NDAC  0   // GPIB 8  : Not Data Accepted  - MCU D2
#define GPIB_IFC   4   // GPIB 9  : Interface Clear    - MCU D4
#define GPIB_SRQ   1   // GPIB 10 : Service request    - MCU D3
#define GPIB_ATN   30  // GPIB 11 : Attention          - MCU D5
// #define GPIB_DIO5  8   // GPIB 13 : I/O data bit 5     - MCU B4
// #define GPIB_DIO6  9   // GPIB 14 : I/O data bit 6     - MCU B5
// #define GPIB_DIO7  10  // GPIB 15 : I/O data bit 7     - MCU B6
// #define GPIB_DIO8  11  // GPIB 16 : I/O data bit 8     - MCU B7
#define GPIB_REN   12  // GPIB 17 : Remote enable      - MCU D6

#endif
