/********************************************************************************
 Name:		ScanAction.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 ScanAction - ++scan command implementation
 Scans the bus for available devices. If the device found, SCPI identification
 command sent and print the identification data. Can be used only in controller mode
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include "ScanAction.h"
#include "GPIB.h"
#include "gpib_mac.h"
#include "ports.h"
#include "SettingCollection.h"

ScanAction::ScanAction()
{
}

ScanAction::ScanAction(GPIB_MAC* gpib_mac)
{
	this->gpib_mac = gpib_mac;
}

void ScanAction::Execute(char* param)
{
	// Loop through the available addresses, except this controller, Sending a SendAddress
	// command, declaring ourself as talker
	// wait for the NDAC ??? signal, if received the device is there
	// Send an identification request SCPI for sure, question if something exists for
	// native IEEE-488.1 or IEEE-488.2
	// Send an UNL
	char* currParam;
	uint16_t i, j;
	uint8_t status;
	bool ScanDeep;
	currParam = strtok(param, " \t");
	if (currParam != NULL)
	{
		ScanDeep = strcmp(param, "deep") == 0;
	}
	char linebuff[73]; // 72 - Maximum lenght of the IDN response according to the IEEE-488.2
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		for (i = 0; i < 31; i++)
		{
			if (i != setting_ADDR_OWN.Get())
			{
				// NDAC is asserted by all of the devices on the bus. No NDAC means empty bus. 
				if (this->gpib_mac->SendAddress(setting_ADDR_OWN.Get(), i) == GPIB_ERR_NDAC_TIMEOUT)
				{
					Serial.println("No Device Present");
					return;
				}
				if (this->gpib_mac->IsListening() == GPIB_OK)
				{
					Serial.print(i);
					Serial.println(": Device Present");
					// this->gpib_mac->SendCommand(GPIB_CMD_UNL);
					// Send SCPI IDN command
					this->SendLine(setting_ADDR_OWN.Get(), i, setting_EOI.Get(), setting_EOS.Get(), "*IDN?");
					// Read line
					// Blocking receive
					gpib_app.GPIBRead(i, false, true, 0, false);
					Serial.println();
					// The HP 3488 has an ID? command. It maybe worth implemented. Also the list of boards installed - maybe better to write a PC software for this?
				}
				this->gpib_mac->SendCommand(GPIB_CMD_UNL);
				// Scan Deep - Secondary Addresses
				if (ScanDeep)
				{
					for (j = 96; j < 127; j++)
					{
						this->gpib_mac->SendAddress(setting_ADDR_OWN.Get(), i + (j << 8));
						if (this->gpib_mac->IsListening() == GPIB_OK)
						{
							Serial.print(i);
							Serial.print(" ");
							Serial.print(j);
							Serial.println(": Device Present");
							// this->gpib_mac->SendCommand(GPIB_CMD_UNL);
							// Send SCPI IDN command
							this->SendLine(setting_ADDR_OWN.Get(), i, setting_EOI.Get(), setting_EOS.Get(), "*IDN?");
							// Read line
							// Blocking receive
							gpib_app.GPIBRead(i + (j << 8), false, true, 0, false);
							Serial.println();
						}
					}
				}
			}
		}
		Serial.println("Scan completted");
	}
	else
	{
		// Error
	}
}

uint8_t ScanAction::SendLine(uint8_t talker, uint8_t listener, uint8_t eoi, uint8_t eos, char* linebuff)
{
	uint16_t i;
	// Controller
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		this->gpib_mac->SendAddress(talker, listener);
		// Send everything, except the last character in the buffer. It needs special treatment
		for (i = 0; i < strlen(linebuff) - 1; i++)
		{
			this->gpib_mac->SendByte(linebuff[i], false);
		}
		// We have to figure out if anything further is sent
		this->gpib_mac->SendByte(linebuff[strlen(linebuff) - 1],
			eoi == 1 &&		// If EOI enabled
			eos == 3		// If nothing further appended to the string
		);
		// Send termination characters
		switch (eos)
		{
		case 0:
			this->gpib_mac->SendByte(13, false);
			this->gpib_mac->SendByte(10, eoi);
			break;
		case 1:
			this->gpib_mac->SendByte(13, eoi);
			break;
		case 2:
			this->gpib_mac->SendByte(10, eoi);
			break;
		default:
			break;
		}
		// Send Unlisten
		this->gpib_mac->SendCommand(GPIB_CMD_UNL);
	}
	return 0; // error handling not yet implemented!!!
}

