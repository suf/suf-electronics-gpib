/********************************************************************************
 Name:		AddressSettingAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 AddressSettingAction - Called by the ++addr command. The address setting command
 works differently in device and controller mode. It sets the remote unit address
 in controller mode and sets its own address in device mode.
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>

#include "AddressSettingAction.h"
#include "SettingCollection.h"
#include "gpib_mac.h"
#include "GPIB_APP.h"

 /// Default constructor
AddressSettingAction::AddressSettingAction() {}
/// Execute the action
/// param - string contains space separated address values. Address and Subaddress
void AddressSettingAction::Execute(char* param)
{
	int32_t value = 0;
	if ((setting_MODE.Get() & GPIB_MODE_CTRL) == GPIB_MODE_CTRL)
	{
		// Controller mode, set the remote address
		if (param != NULL)
		{
			value = setting_ADDR_REM.Set(param);
			if (value < 0)
			{
				setting_ADDR_REM.ErrorOutOfRange(CMD_addr);
			}
		}
		else
		{
			setting_ADDR_REM.PrintValue();
		}
	}
	else
	{
		// Device mode, set own address
		setting_ADDR_OWN.Set(param);
		if (param != NULL)
		{
			value = setting_ADDR_OWN.Set(param);
			if (value < 0)
			{
				setting_ADDR_OWN.ErrorOutOfRange(CMD_addr);
			}
		}
		else
		{
			setting_ADDR_OWN.PrintValue();
		}
	}
}
