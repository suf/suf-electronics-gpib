/********************************************************************************
 Name:		debug_serial.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 debug_serial - Serial debug object
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once

#include <Arduino.h>
#include <stdint.h>

#include "debug.h"

class DEBUG_SERIAL : public DEBUG
{
public:
	DEBUG_SERIAL();
	DEBUG_SERIAL(uint32_t baud);
	DEBUG_SERIAL(uint32_t baud, uint8_t severity);
	void begin();
	void begin(uint8_t severity);
	void msg(uint8_t severity, char* message);
	void msg(uint8_t severity, uint8_t msgid, uint8_t moduleId, uint8_t methodId);
	void msg(uint8_t severity, uint8_t msgid, uint8_t moduleId, uint8_t methodId, int32_t value);
	void PrintMethod(uint8_t moduleId, uint8_t methodId);
	void Clear();
	uint32_t baud;
};
