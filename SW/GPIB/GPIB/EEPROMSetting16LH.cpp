/********************************************************************************
 Name:		EEPROMSetting16LH.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 EEPROMSetting16LH - Setting value store with one word (separate low/high) EEPROM backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>
#include <EEPROM.h>
#include "GPIB_APP.h"
#include "Setting.h"
#include "EEPROMSetting16LH.h"
#include "SettingCollection.h"


EEPROMSetting16LH::EEPROMSetting16LH()
{
}

EEPROMSetting16LH::EEPROMSetting16LH(uint8_t addr, uint8_t def_low, uint8_t def_high)
{
	this->EEPROMaddr = addr;
	this->def_low = def_low;
	this->def_high = def_high;
}

bool EEPROMSetting16LH::begin()
{
	this->value = EEPROM.read(this->EEPROMaddr + 1);
	this->value <<= 8;
	this->value += EEPROM.read(this->EEPROMaddr);
	return true;
}

void EEPROMSetting16LH::InitStore()
{
	this->value = this->def_high;
	this->value <<= 8;
	this->value += this->def_low;
	this->Save();
}

int32_t EEPROMSetting16LH::Parse(char* param)
{
	char* currParam;
	int32_t retValue;
	uint16_t currValue;
	currParam = strtok(param, " \t");
	if (currParam != NULL)
	{
		currValue = atoi(currParam); // Low byte
		retValue = currValue & 0xFF;
		currParam = strtok(NULL, " \t");
		if (currParam != NULL)
		{
			// Highbyte present
			currValue = atoi(currParam); // High byte
			retValue += (currValue << 8);
		}
	}
	else
	{
		return -1;
	}
	return retValue;
}

int32_t EEPROMSetting16LH::Set(uint8_t value_low, uint8_t value_high)
{
	this->value = value_high;
	this->value <<= 8;
	this->value += value_low;
	if (setting_AUTOSAVE.Get() == 1)
	{
		this->Save();
	}
	return this->value;
}

int32_t EEPROMSetting16LH::Set(uint16_t value)
{
	return this->Set(value & 0x00FF, value >> 8);
}

int32_t EEPROMSetting16LH::Set(char* param)
{
	return this->Set(this->Parse(param));
}

void EEPROMSetting16LH::PrintValue()
{
	Serial.print(this->value & 0x00FF);
	Serial.print(".");
	Serial.println(this->value >> 8);
}

void EEPROMSetting16LH::Save()
{
	EEPROM.update(this->EEPROMaddr, this->value & 0xFF);
	EEPROM.update(this->EEPROMaddr + 1, (this->value >> 8) & 0xFF);
}
