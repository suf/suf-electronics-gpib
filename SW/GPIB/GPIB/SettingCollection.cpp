/********************************************************************************
 Name:		SettingsCollection.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 SettingsCollection - Collection of the instantiated settings and action objects
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>

#include "gpib_mac.h"
#include "SettingCollection.h"
#include "GPIB.h"

Setting setting_AUTO(0, 0, 1);
EEPROMSetting8 setting_AUTOSAVE(SETTINGS_ADDR_AUTOSAVE, true, 0, 1);
EEPROMSetting8 setting_MODE(SETTINGS_ADDR_MODE, GPIB_MODE_CTRL, 0, 1);
EEPROMSetting8 setting_ADDR_OWN(SETTINGS_ADDR_ADDR_OWN, 5, 0, 30);
EEPROMSettingAddr setting_ADDR_REM(SETTINGS_ADDR_ADDR_REM, 0, 0);
EEPROMSetting8 setting_EOI(SETTINGS_ADDR_EOI, 1, 0, 1);
EEPROMSetting8 setting_EOS(SETTINGS_ADDR_EOS, 1, 0, 1);
EEPROMSetting8 setting_EOT_ENABLE(SETTINGS_ADDR_EOT_ENABLE, false, 0, 1);
EEPROMSetting8 setting_EOT_CHAR(SETTINGS_ADDR_EOT_CHAR, 10, 0, 255);
EEPROMSetting8 setting_LISTEN_ONLY(SETTINGS_ADDR_LISTEN_ONLY, false, 0, 1);
EEPROMSetting16 setting_READ_TIMEOUT(SETTINGS_ADDR_READ_TIMEOUT, 200, 1, 3000);
EEPROMSetting8 setting_STATUS(SETTINGS_ADDR_STATUS, 0, 0, 255);
EEPROMSetting8 setting_SERIAL_ECHO(SETTINGS_ADDR_SERIAL_ECHO, false, 0, 1);
EEPROMSetting8 setting_DBG_LEVEL(SETTINGS_ADDR_DBG_LEVEL, 9, 0, 9);
EEPROMSetting8 setting_LON_FORMAT(SETTINGS_ADDR_LON_FORMAT, 0, 0, 2);
EEPROMSetting16LH setting_EMU_VER(SETTINGS_ADDR_EMULATE_VER, 0, 0);
// EEPROMSetting8 setting_SYSCTRL(SETTINGS_ADDR_SYSCTRL, GPIB_MODE_SYSCTRL >> 1, 0, 1);

GPIBModeSettingAction action_MODE_SETTING(&gpib_mac);
