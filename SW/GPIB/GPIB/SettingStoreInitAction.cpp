/********************************************************************************
 Name:		SettingStoreInitAction.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 SettingStoreInitAction - Initialize the setting store to the factory default values
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "SettingStoreInitAction.h"
#include "GPIB_APP.h"


SettingStoreInitAction::SettingStoreInitAction()
{
}

void SettingStoreInitAction::Execute(char* param)
{
	for (uint8_t i = 0; i < CmdArr_len; i++)
	{
		if (commands[i].setting != NULL)
		{
			commands[i].setting->InitStore();
		}
	}
}


