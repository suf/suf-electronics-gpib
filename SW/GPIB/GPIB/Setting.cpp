/********************************************************************************
 Name:		Setting.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 Setting -  Setting base object and value store with one word in-memory backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "Setting.h"
#include "GPIB_APP.h"

#include "Messages.h"

Setting::Setting()
{
	this->isNull = true;
}

Setting::Setting(uint16_t defaultValue, uint16_t min, uint16_t max)
{
	this->defaultValue = defaultValue;
	this->min = min;
	this->max = max;
}

bool Setting::begin()
{
	if (this->Validate(this->defaultValue))
	{
		this->value = this->defaultValue;
		return true;
	}
	return false;
}

int32_t Setting::Parse(char* param)
{
	char* currParam;
	currParam = strtok(param, " \t");
	return (currParam != NULL) ? atoi(currParam) : -1;
}

uint16_t Setting::Get()
{
	return this->value;
}

int32_t Setting::Set(char* param)
{
	return this->Set(this->Parse(param));
}

int32_t Setting::Set(uint16_t value)
{
	if (this->Validate(value))
	{
		this->value = value;
		return value;
	}
	return -1;
}

bool Setting::Validate(uint16_t value)
{
	return (value >= this->min && value <= this->max);
}

void Setting::InitStore()
{
	this->value = this->defaultValue;
}

void Setting::ErrorOutOfRange(uint8_t cmd_id)
{
	char buffer[20];
	strcpy_P(buffer, (char*)pgm_read_word(&(CmdStr[cmd_id])));
	PrintMessage(MESSAGE_Error_colon);
	Serial.print(buffer);
	PrintMessage(MESSAGE_Out_of_range);
	Serial.print(min);
	Serial.print("-");
	Serial.print(max);
	Serial.println("]");
}

void Setting::PrintSetting(uint8_t cmd_id)
{
	char buffer[20];
	strcpy_P(buffer, (char*)pgm_read_word(&(CmdStr[cmd_id])));
	Serial.print(buffer);
	Serial.print(": ");
	this->PrintValue();
}

void Setting::PrintValue()
{
	Serial.println(this->value);
}

void Setting::Save() {}
