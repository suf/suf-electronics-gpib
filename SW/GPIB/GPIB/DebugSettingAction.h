/********************************************************************************
 Name:		DebugSettingAction.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 DebugSettingAction - Called by the ++dbg_setting command. In addition to save the
 setting to the EEPROM also set the level in the debug object.
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>
#include "Action.h"
#include "debug.h"
class DebugSettingAction : public Action
{
public:
	/// Default constructor
	DebugSettingAction(DEBUG* dbg);
	/// Execute the action
	/// param - string contains the debug level to set (0 - 9).
	void Execute(char* param);
private:
	DEBUG* dbg;
};

