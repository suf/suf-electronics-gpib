/********************************************************************************
 Name:		EEPROMSettingAddr.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 EEPROMSettingAddr - Setting value store for GPIB Pri/Sub address EEPROM backend
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>
#include <EEPROM.h>
#include "Setting.h"
#include "EEPROMSettingAddr.h"
#include "SettingCollection.h"

EEPROMSettingAddr::EEPROMSettingAddr()
{
}

EEPROMSettingAddr::EEPROMSettingAddr(uint8_t addr, uint8_t def_addr, uint8_t def_subaddr)
{
	this->EEPROMaddr = addr;
	this->def_addr = def_addr;
	this->def_subaddr = def_subaddr;
}

bool EEPROMSettingAddr::begin()
{
	uint16_t tmpValue;
	tmpValue = EEPROM.read(this->EEPROMaddr + 1);
	tmpValue <<= 8;
	tmpValue += EEPROM.read(this->EEPROMaddr);
	if (this->Validate(tmpValue))
	{
		this->value = tmpValue;
		return true;
	}
	else
	{
		this->Set(this->def_addr, this->def_subaddr);
	}
	return false;
}

void EEPROMSettingAddr::InitStore()
{
	this->value = this->def_subaddr;
	this->value <<= 8;
	this->value += this->def_addr;
	this->Save();
}

int32_t EEPROMSettingAddr::Parse(char* param)
{
	char* currParam;
	int32_t retValue = -1;
	uint16_t currAddr;
	currParam = strtok(param, " \t");
	if (currParam != NULL)
	{
		currAddr = atoi(currParam); //Address
		retValue = currAddr & 0xFF;
		currParam = strtok(NULL, " \t");
		if (currParam != NULL)
		{
			// SubAddress present
			currAddr = atoi(currParam); //SubAddress
			retValue += currAddr << 8;
		}
	}
	return retValue;
}

int32_t EEPROMSettingAddr::Set(uint8_t addr, uint8_t subaddr)
{
	if (this->Validate(addr, subaddr))
	{
		this->value = subaddr;
		this->value <<= 8;
		this->value += addr;
		if (setting_AUTOSAVE.Get() == 1)
		{
			this->Save();
		}
		return this->value;
	}
	return -1;
}

int32_t EEPROMSettingAddr::Set(uint16_t value)
{
	return this->Set(value & 0xFF, value >> 8);
}

int32_t EEPROMSettingAddr::Set(char* param)
{
	int32_t addr;
	addr = this->Parse(param);
	if (addr > 0)
	{
		return this->Set(addr);
	}
	return -1;
}

bool EEPROMSettingAddr::Validate(uint16_t value)
{
	return this->Validate(value & 0x00FF, value >> 8);
}

bool EEPROMSettingAddr::Validate(uint8_t addr, uint8_t subaddr)
{
	return addr >= 0 && addr <= 30 && (subaddr == 0 || (subaddr >= 96 && subaddr <= 126));
}

void EEPROMSettingAddr::PrintValue()
{
	Serial.print(this->value & 0x00FF);
	Serial.print(" ");
	Serial.println(this->value >> 8);
}

void EEPROMSettingAddr::Save()
{
	EEPROM.update(this->EEPROMaddr, this->value & 0xFF);
	EEPROM.update(this->EEPROMaddr + 1, (this->value >> 8) & 0xFF);
}


