/********************************************************************************
 Name:		GPIB_APP.cpp
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 GPIB_APP - Main application object. Handle the serial commands and the GPIB communication
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#include <Arduino.h>
#include <stdint.h>

#include "GPIB.h"
#include "GPIB_APP.h"
#include "gpib_mac.h"
#include "debug.h"
#include "Messages.h"

#include "Command.h"
#include "SettingCollection.h"

#include "Action.h"
#include "AddressSettingAction.h"
#include "DebugSettingAction.h"
#include "GPIBCommandAction.h"
#include "GPIB_LLO_Action.h"
#include "ListSettingAction.h"
#include "SettingStoreInitAction.h"
#include "SaveSettingsAction.h"
#include "GPIB_IFC_Action.h"
#include "ResetAction.h"
#include "SRQAction.h"
#include "PrintVersionAction.h"
#include "GPIBReadAction.h"
#include "GPIBReadHexAction.h"
#include "ScanAction.h"
#include "TriggerAction.h"
#include "SpollAction.h"

#include "Setting.h"
#include "EEPROMSetting8.h"
#include "EEPROMSetting16.h"
#include "EEPROMSettingAddr.h"


const char CmdStr_addr[] PROGMEM = "addr";
const char CmdStr_auto[] PROGMEM = "auto";
const char CmdStr_clr[] PROGMEM = "clr";
const char CmdStr_eoi[] PROGMEM = "eoi";
const char CmdStr_eos[] PROGMEM = "eos";
const char CmdStr_eot_enable[] PROGMEM = "eot_enable";
const char CmdStr_eot_char[] PROGMEM = "eot_char";
const char CmdStr_ifc[] PROGMEM = "ifc";
const char CmdStr_llo[] PROGMEM = "llo";
const char CmdStr_loc[] PROGMEM = "loc";
const char CmdStr_lon[] PROGMEM = "lon";
const char CmdStr_mode[] PROGMEM = "mode";
const char CmdStr_read[] PROGMEM = "read";
const char CmdStr_read_tmo_ms[] PROGMEM = "read_tmo_ms";
const char CmdStr_rst[] PROGMEM = "rst";
const char CmdStr_savecfg[] PROGMEM = "savecfg";
const char CmdStr_spoll[] PROGMEM = "spoll";
const char CmdStr_srq[] PROGMEM = "srq";
const char CmdStr_status[] PROGMEM = "status";
const char CmdStr_trg[] PROGMEM = "trg";
const char CmdStr_ver[] PROGMEM = "ver";
const char CmdStr_help[] PROGMEM = "help";
const char CmdStr_dcl[] PROGMEM = "dcl";
const char CmdStr_verbose[] PROGMEM = "verbose";
const char CmdStr_addr_rem[] PROGMEM = "addr_rem";
const char CmdStr_addr_own[] PROGMEM = "addr_own";
const char CmdStr_dbg_level[] PROGMEM = "dbg_level";
const char CmdStr_echo[] PROGMEM = "echo";
const char CmdStr_tct[] PROGMEM = "tct";
const char CmdStr_eeprom_init[] PROGMEM = "eeprom_init";
const char CmdStr_emulate_ver[] PROGMEM = "emulate_ver";
const char CmdStr_lon_format[] PROGMEM = "lon_format";
const char CmdStr_send_srq[] PROGMEM = "send_srq";
const char CmdStr_list_settings[] PROGMEM = "list_settings";
const char CmdStr_scan[] PROGMEM = "scan";
const char CmdStr_sys_ctrl[] PROGMEM = "sys_ctrl";
const char CmdStr_read_hex[] PROGMEM = "read_hex";

const char* const CmdStr[] PROGMEM= {
	CmdStr_addr,
	CmdStr_auto,
	CmdStr_clr,
	CmdStr_eoi,
	CmdStr_eos,
	CmdStr_eot_enable,
	CmdStr_eot_char,
	CmdStr_ifc,
	CmdStr_llo,
	CmdStr_loc,
	CmdStr_lon,
	CmdStr_mode,
	CmdStr_read,
	CmdStr_read_tmo_ms,
	CmdStr_rst,
	CmdStr_savecfg,
	CmdStr_spoll,
	CmdStr_srq,
	CmdStr_status,
	CmdStr_trg,
	CmdStr_ver,
	CmdStr_help,

	CmdStr_dcl,
	CmdStr_verbose,

	CmdStr_addr_rem,
	CmdStr_addr_own,
	CmdStr_dbg_level,
	CmdStr_echo,
	CmdStr_tct,
	CmdStr_eeprom_init,
	// version emulate
	CmdStr_emulate_ver,
	// various listen only modes for promiscous mode
	CmdStr_lon_format,
	// send srq (the srq is only for getting the incomming SRQ status)
	CmdStr_send_srq,
	// List all of the EEPROM (and working?) settings
	CmdStr_list_settings,
	// Scan the bus
	CmdStr_scan,
	CmdStr_read_hex
	// System controller mode, have to be handled separately from the mode command for compatibility reason
	// the system controller mode forced to switch on and off if the device/controller mode swiched by command
	// the system controller mode will be kept intact when the controller mode handed over or received via TCT
	// CmdStr_sys_ctrl
	// buffering - how the GPIB out buffering is handled
};


/// ListSettingAction actionListSetting(1);

Command commands[CmdArr_len] = {
		Command(CMD_addr, new AddressSettingAction()),
		Command(CMD_auto, &setting_AUTO),
		Command(CMD_clr, new GPIBCommandAction(&gpib_mac, GPIB_CMD_SDC, GPIB_MODE_CTRL, GPIB_CMD_ADDRESSED)) ,
		Command(CMD_eoi, &setting_EOI) ,
		Command(CMD_eos, &setting_EOS) ,
		Command(CMD_eot_enable, &setting_EOT_ENABLE) ,
		Command(CMD_eot_char, &setting_EOT_CHAR) ,
		Command(CMD_ifc, new GPIB_IFC_Action(&gpib_mac)),
		Command(CMD_llo, new GPIB_LLO_Action(&gpib_mac)),
		Command(CMD_loc, new GPIBCommandAction(&gpib_mac, GPIB_CMD_GTL, GPIB_MODE_CTRL, GPIB_CMD_ADDRESSED)),
		Command(CMD_lon, &setting_LISTEN_ONLY),
		Command(CMD_mode, &setting_MODE, &action_MODE_SETTING),	// Action required to change the underlaying gpib_mac. The gpib_mac setting must be used everywhere to able to handle the TCT status
		Command(CMD_read, new GPIBReadAction()),
		Command(CMD_read_tmo_ms, &setting_READ_TIMEOUT),
		Command(CMD_rst, new ResetAction()),
		Command(CMD_savecfg, &setting_AUTOSAVE, new SaveSettingsAction()),
		Command(CMD_spoll, new SpollAction(&gpib_mac)),
		Command(CMD_srq, new SRQAction(&gpib_mac)),
		Command(CMD_status, &setting_STATUS),
		Command(CMD_trg, new TriggerAction(&gpib_mac)),
		Command(CMD_ver, new PrintVersionAction()),
		Command(CMD_help),

		Command(CMD_dcl, new GPIBCommandAction(&gpib_mac, GPIB_CMD_DCL, GPIB_MODE_CTRL, GPIB_CMD_UNADDRESSED)),
		Command(CMD_verbose),

		Command(CMD_addr_rem, &setting_ADDR_REM),
		Command(CMD_addr_own, &setting_ADDR_OWN),
		Command(CMD_dbg_level, &setting_DBG_LEVEL, new DebugSettingAction(&slog)),
		Command(CMD_echo, &setting_SERIAL_ECHO),
		Command(CMD_tct),
		Command(CMD_eeprom_init, new SettingStoreInitAction()),
		// version emulate
		Command(CMD_emulate_ver, &setting_EMU_VER),
		// various listen only modes for promiscous mode
		Command(CMD_lon_format, &setting_LON_FORMAT),
		// send srq (the srq is only for getting the incomming SRQ status)
		Command(CMD_send_srq),
		// List all of the EEPROM (and working?) settings
		Command(CMD_list_settings, new ListSettingAction()),
		// Scan the bus for available devices
		Command(CMD_scan,new ScanAction(&gpib_mac)),
		Command(CMD_read_hex, new GPIBReadHexAction())
		// Command(CMD_sys_ctrl,&setting_SYSCTRL),
		// buffering - how the GPIB out buffering is handled TBD !!!

		// Command profile
		// Set the EEPROM settings to a certain profile:
		// - native - Optimized for this interface
		// - prologix - Emulate Prologix controller - like ver string
		// - console - Optimized for console access - like local echo
};

GPIB_APP::GPIB_APP(GPIB_MAC* gpib_mac, DEBUG* log)
{
	this->gpib_mac = gpib_mac;
	this->log = log;
}

void GPIB_APP::begin()
{
	uint8_t i;
	this->Log(LOG_SEVERITY_INFO, LOG_METHOD_begin, MESSAGE_Init);
	for (i = 0; i < CmdArr_len; i++)
	{
		if (commands[i].setting != NULL)
		{
			if (!commands[i].setting->begin())
			{
				Serial.println(i);
				break;
			}
		}
	}
	// initialization failed, EEPROM data error, EEPROM set back to factory default
	if (i < CmdArr_len)
	{
		for (i = 0; i < CmdArr_len; i++)
		{
			if (commands[i].setting != NULL)
			{
				commands[i].setting->InitStore();
			}
		}
		Serial.println("EEPROM Initialized");
	}
}

void GPIB_APP::ReceiveCharSerial()
{
	int i;
	char* cmdbuff;
	char* cmd;

	this->linebuff[this->linebuff_pos] = Serial.read();

	if (setting_SERIAL_ECHO.Get())
	{
		// local echo
		Serial.print(this->linebuff[this->linebuff_pos]);
	}
	// the current solution works fine with human, but for binary data transfer, may some modifications needed
	switch (this->linebuff[this->linebuff_pos])
	{
		// Escape & Backspace handling !!!
	case 127:	// This is the Backspace sent by the PUTTY
	case BS:	// BackSpace
		if (this->linebuff_pos > 0)
		{
			this->linebuff_pos--;
		}
		break;
	case LF:
	case CR:
	case 0:
		if (setting_SERIAL_ECHO.Get())
		{
			// local echo
			Serial.println();
		}
		// line termination
		if (this->linebuff_pos > 0)
		{
			// process buffer
			if (this->linebuff_pos > 1 && this->linebuff[0] == '+' && this->linebuff[1] == '+')
			{
				// controller command
				// cmdbuff = this->linebuff + 2;
				this->linebuff[this->linebuff_pos] = 0;
				cmd = strtok(this->linebuff + 2, " \t");
				if (cmd != NULL)
				{
					this->ProcessCommand(cmd, (strlen(cmd) + 2 < this->linebuff_pos) ? cmd + strlen(cmd) + 1 : NULL);
				}
			}
			else
			{
				// payload, send directly to the addressed device
				if (this->linebuff_pos > 0)
				{
					this->GPIBSendString();
					if ((this->gpib_mac->GetMode() & GPIB_MODE_CTRL) && setting_AUTO.Get())
					{
						// In controller mode, auto enabled, a read after write need to be issued
						this->GPIBStartReceive();
					}
				}
			}
			// end processing, clear buffer
			this->linebuff_pos = 0;
		}
		break;
	default:
		this->linebuff_pos++;
		break;
	}
}

void GPIB_APP::GPIBListenWorker()
{
	uint16_t receiveData;
	if (this->read_listen)
	{
		if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
		{
			// Controller
			if (this->gpib_mac->Available())
			{
				// Read the data
				receiveData = this->gpib_mac->Read();
				// Send the data to the Serial Port - Unbuffered
				if (this->read_hex)
				{
					if(!this->read_firstchar) Serial.print(" ");
					if((receiveData & 0x00FF) < 0x0010) Serial.print("0");
					Serial.print((char)(receiveData & 0x00FF), HEX);
				}
				else
				{
					Serial.print((char)(receiveData & 0x00FF));
				}
				if
				(
					(receiveData & 0x0100) && this->read_eoi // Eoi signaled and enabled
					|| (((receiveData & 0x00FF) == this->read_EndChar) && this->read_IsParam) // call parametrized and endchar reached
				)
				{
					// End of read
					this->read_listen = false;
					this->gpib_mac->SendCommand(GPIB_CMD_UNT);
					// Handle EOT char, if enabled
					if (setting_EOT_ENABLE.Get())
					{
						Serial.print((char)setting_EOT_CHAR.Get());
					}
					// Add a newline, if using read_hex and echo enabled, for readability on the console
					if ((receiveData & 0x0100) && this->read_eoi && this->read_hex && setting_SERIAL_ECHO.Get())
					{
						Serial.println();
					}
				}
				else
				{
					this->gpib_mac->StartRead();
					this->read_timeout = millis() + setting_READ_TIMEOUT.Get();
				}
				this->read_firstchar = false;
			}
			if (this->read_timeout < millis())
			{
				// timeout, close the communication
				this->read_listen = false;
				this->gpib_mac->SendCommand(GPIB_CMD_UNT);
			}
			// Serial CRLF - Serial line termination need to be configurable ???
		}
		else
		{
			// Device
		}
	}
}

uint8_t GPIB_APP::GPIBSendString()
{
	uint16_t i;
	// Controller
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		this->gpib_mac->SendAddress(setting_ADDR_OWN.Get(),setting_ADDR_REM.Get());
		// Send everything, except the last character in the buffer. It needs special treatment
		for (i = 0; i < this->linebuff_pos - 1; i++)
		{
			this->gpib_mac->SendByte(this->linebuff[i], false);
		}
		this->gpib_mac->SendByte(this->linebuff[linebuff_pos - 1],
			setting_EOI.Get() == 1 &&	// If EOI enabled
			setting_EOS.Get() == 3		// If nothing further appended to the string
		);

		// Send termination characters
		switch (setting_EOS.Get())
		{
		case 0:
			this->gpib_mac->SendByte(13, false);
			this->gpib_mac->SendByte(10, setting_EOI.Get());
			break;
		case 1:
			this->gpib_mac->SendByte(13, setting_EOI.Get());
			break;
		case 2:
			this->gpib_mac->SendByte(10, setting_EOI.Get());
			break;
		default:
			break;
		}
		// Send Unlisten
		this->gpib_mac->SendCommand(GPIB_CMD_UNL);
		// Auto mode
		if (setting_AUTO.Get())
		{
			this->GPIBStartReceive(); // Start receive EOI, no endchar
		}
	}
	// Device
	else
	{

	}
	return 0; // error handling not yet implemented!!!
}

// Not parametrized implementation, used in auto mode (++auto)
// where eoi is used and the read end is not parametrized
uint8_t GPIB_APP::GPIBStartReceive()
{
	return this->GPIBStartReceive(false, true, 0, false);
}

uint8_t GPIB_APP::GPIBStartReceive(bool IsParam, bool eoi, uint8_t EndChar)
{
	return this->GPIBStartReceive(IsParam, eoi, EndChar, false);
}

uint8_t GPIB_APP::GPIBStartReceive(bool IsParam, bool eoi, uint8_t EndChar, bool hex)
{
	return this->GPIBStartReceive(setting_ADDR_REM.Get(), IsParam, eoi, EndChar, hex);
}

uint8_t GPIB_APP::GPIBStartReceive(uint16_t rem_addr, bool IsParam, bool eoi, uint8_t EndChar, bool hex)
{
	if (this->gpib_mac->GetMode() & GPIB_MODE_CTRL)
	{
		// Controller Mode
		this->read_IsParam = IsParam;
		this->read_eoi = eoi;
		this->read_EndChar = EndChar;
		this->read_hex = hex;
		this->read_firstchar = true;

		this->gpib_mac->SendAddress(rem_addr, setting_ADDR_OWN.Get());
		this->gpib_mac->StartRead();
		this->read_timeout = millis() + setting_READ_TIMEOUT.Get();
		// Switch listen mode on
		this->read_listen = true;
	}
	else
	{
		// Device mode
	}
	return 0; // error handling not yet implemented!!!
}

bool GPIB_APP::GPIBIsListening()
{
	return this->read_listen;
}

void GPIB_APP::ProcessCommand(char* cmd, char* params)
{
	int i;
	for (i = 0; i < CmdArr_len; i++)
	{
		if (strcmp_P(cmd, (char*)pgm_read_word(&(CmdStr[i]))) == 0)
		{
			commands[i].Execute(params);
		}
	}
}

// Internal implementation of the debug log
void GPIB_APP::Log(uint8_t severity, uint8_t methodId, uint8_t msgid)
{
	if (this->log != NULL)
	{
		this->log->msg(severity, msgid, LOG_MODULE_GPIB_APP, methodId);
	}
}

// Blocking mode reader
void GPIB_APP::GPIBRead(uint16_t rem_addr, bool IsParam, bool eoi, uint8_t EndChar, bool hex)
{
	this->GPIBStartReceive(rem_addr, IsParam, eoi, EndChar, hex);
	// Start the listenworker in blocking mode
	while (this->read_listen)
	{
		this->GPIBListenWorker();
	}
}