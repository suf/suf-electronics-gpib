/********************************************************************************
 Name:		debug.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 debug - Base object for debug logging. Only serial logging implemented so far
 Reference:
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/

#pragma once

#include <Arduino.h>
#include <stdint.h>

// Log severity levels inherited from syslog service
#define LOG_SEVERITY_EMERG   0
#define LOG_SEVERITY_ALERT   1
#define LOG_SEVERITY_CRIT    2
#define LOG_SEVERITY_ERR     3
#define LOG_SEVERITY_WARNING 4
#define LOG_SEVERITY_NOTICE  5
#define LOG_SEVERITY_INFO    6
#define LOG_SEVERITY_DEBUG   7

#define LOG_SEVERITY_DEFAULT 3

// The label array for log severity levels
extern const char* const DbgStr[];
extern const char* const DbgModule[];
#define LOG_MODULE_GPIB_MAC		0
#define LOG_MODULE_MAIN			1
#define LOG_MODULE_GPIB_APP		2

extern const char* const DbgMethod[];
#define LOG_METHOD_SetMode		0
#define LOG_METHOD_SendByte		1
#define LOG_METHOD_SendAddress	2
#define LOG_METHOD_IsListening	3
#define LOG_METHOD_begin		4
#define LOG_METHOD_setup		5
#define LOG_METHOD_loop			6

class DEBUG
{
public:
	DEBUG();
	virtual void msg(uint8_t severity, char* message);
	virtual void msg(uint8_t severity, uint8_t modId, uint8_t methodId, uint8_t msgid);
	virtual void msg(uint8_t severity, uint8_t modId, uint8_t methodId, uint8_t msgid, int32_t value);
	void PrintMethod(uint8_t moduleId, uint8_t methodId);
	void SetDebugLevel(uint8_t severity);
protected:
	uint8_t severity;
};
