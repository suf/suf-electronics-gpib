/********************************************************************************
 Name:		SettingsCollection.h
 Author:	SUF
 Description:
 Arduino based USB/Serial - IEEE-488 interface
 (Known as GP-IB, HP-IB)
 SettingsCollection - Collection of the instantiated settings and action objects
 Prologix GP-IB USB Controller:
 https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf
 IEEE-488.1-2003 Standard:
 https://standards.ieee.org/standard/488_1-2003.html
 SCPI Specification:
 http://www.ivifoundation.org/docs/scpi-99.pdf
 Special thanks to Emanuele Girlando for the inspiration of this work:
 http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html
 License:
 This work is licensed under a Creative Commons
 Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license
 https://creativecommons.org/licenses/by-nc-sa/4.0/
 ********************************************************************************/
#pragma once
#include <Arduino.h>

#include "Setting.h"
#include "EEPROMSetting8.h"
#include "EEPROMSetting16.h"
#include "EEPROMSettingAddr.h"
#include "EEPROMSetting16LH.h"
#include "GPIBModeSettingAction.h"

#define SETTINGS_ADDR_AUTOSAVE			0
#define SETTINGS_ADDR_MODE				1
#define SETTINGS_ADDR_ADDR_OWN			2
#define SETTINGS_ADDR_ADDR_REM			3
#define SETTINGS_ADDR_EOI				5
#define SETTINGS_ADDR_EOS				6
#define SETTINGS_ADDR_EOT_ENABLE		7
#define SETTINGS_ADDR_EOT_CHAR			8
#define SETTINGS_ADDR_LISTEN_ONLY		9
#define SETTINGS_ADDR_READ_TIMEOUT		10
#define SETTINGS_ADDR_STATUS			12
#define SETTINGS_ADDR_SERIAL_ECHO		13

#define SETTINGS_ADDR_DBG_LEVEL			14
#define SETTINGS_ADDR_LON_FORMAT		15
#define SETTINGS_ADDR_EMULATE_VER		16

extern Setting setting_AUTO;
extern EEPROMSetting8 setting_AUTOSAVE;
extern EEPROMSetting8 setting_MODE;
extern EEPROMSetting8 setting_ADDR_OWN;
extern EEPROMSettingAddr setting_ADDR_REM;
extern EEPROMSetting8 setting_EOI;
extern EEPROMSetting8 setting_EOS;
extern EEPROMSetting8 setting_EOT_ENABLE;
extern EEPROMSetting8 setting_EOT_CHAR;
extern EEPROMSetting8 setting_LISTEN_ONLY;
extern EEPROMSetting16 setting_READ_TIMEOUT;
extern EEPROMSetting8 setting_STATUS;
extern EEPROMSetting8 setting_SERIAL_ECHO;
extern EEPROMSetting8 setting_DBG_LEVEL;
extern EEPROMSetting8 setting_LON_FORMAT;
extern EEPROMSetting16LH setting_EMU_VER;
extern GPIBModeSettingAction action_MODE_SETTING;

