# AVR GPIB-USB

Arduino based USB-GPIB Interface. Author: SUF
Based on ATMEGA328P + CH340G (Will be migrated to ATMEGA32U4)

Can be used to connect HP-IB, GP-IB, IEEE-488 based intruments to your computer via USB Port

## Hardware
- [x] V1.0  - Original implementation (has some flaws - doesn't interfare with the usability)
- [x] V1.0a - Minor changes (flaws of the v1.0 corrected)
- [x] V1.1  - Two sided USB connector/ISP connector placement (allow to use either Male or Female GPIB connector)
- [ ] V2.0  - ATMEGA32U4 based design
Unhandled bugs, features to add, missing pieces
- [ ] Proper bus driver for using multiple devices on the bus
- [ ] Display and status indicators
- [ ] Ethernet (LXI) ???
- [ ] PoE ???
- [ ] Wi-Fi ???
- [ ] Bluetooth ???
- [ ] IEC-625 hardware implementation (DB-25)

## Software
The software will be kept unreleased until it is mature enough
(not in the public repository, if you intend to help in the development, please contact me)
The hardware is compatible with the [AR488](https://github.com/Twilight-Logic/AR488) implementation what can be used
as alternative firmware for the v1.x hardware design

The command set is based on the Prologix implementation, written mainly in Arduino C++

Commands (The implemented ones are checked):

Prologix commands:

- [x] addr
- [x] auto
- [x] clr
- [x] eoi
- [x] eos
- [x] eot_enable
- [x] eot_char
- [x] ifc
- [x] llo
- [x] loc
- [x] lon
- [x] mode
- [x] read
- [x] read_tmo_ms
- [x] rst
- [x] savecfg
- [x] spoll
- [x] srq
- [x] status
- [x] trg
- [x] ver
- [ ] help

Custom commands:

- [x] dcl
- [ ] verbose
- [x] addr_rem
- [x] addr_own
- [x] dbg_level
- [x] echo
- [ ] tct
- [x] eeprom_init
- [x] emulate_ver
- [x] lon_format
- [ ] send_srq
- [x] list_settings
- [x] scan
- [x] read_hex

Unhandled bugs, features to add, missing pieces
Software:
- [ ] Device mode
- [x] Subaddressing
- [x] SCPI Processing for the bus scan
- [ ] Profile handling not implemented. Adding default profile for Prologix, Console, and Native settings
- [ ] V2.0 hardware support
- [ ] System controller mode, TCT, to handle more than one controller on the bus
- [ ] Debug messages
- [ ] read_hex command configurability
- [ ] USBTMC with USB488 subclass (this is not a low hanging fruit, I know) for VISA compatibility
Hardware:
- [ ] Proper GPIB Bus driver ICs
- [ ] RTS/CTS handling on CH340G

## Hardware
- V1.0  - Original implementation (has some flows - doesn't interfare with the usability)
- V1.0a - Minor changes (flows of the v1.0 corrected)
- V1.1  - Two sided USB connector/ISP connector placement (allow to use either Male or Female GPIB connector)
- V2.0  - ATMEGA32U4 based design

## Reference
[Prologix GP-IB USB Controller](https://prologix.biz/downloads/PrologixGpibUsbManual-6.0.pdf)
[IEEE-488.1-2003 Standard](https://standards.ieee.org/standard/488_1-2003.html)
[SCPI Specification](http://www.ivifoundation.org/docs/scpi-99.pdf)

Special thanks to Emanuele Girlando for the inspiration of this work:
[The original article](http://egirland.blogspot.com/2014/03/arduino-uno-as-usb-to-gpib-controller.html)

License:
[This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) license](https://creativecommons.org/licenses/by-nc-sa/4.0/)